package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class AnotherTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        for (T elem : new HashSet<>(list)) if (Collections.frequency(list, elem) == 3) return elem;

        return null;
    }

}
