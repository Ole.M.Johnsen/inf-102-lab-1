package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T, Integer> map = new HashMap<>();

        for (T elem : list) {
            if (map.containsKey(elem)) {
                if (map.get(elem) == 2) {
                    return elem;
                }

                map.put(elem, map.get(elem) + 1);
            } else {
                map.put(elem, 1);
            }
        }

        return null;
    }

}
